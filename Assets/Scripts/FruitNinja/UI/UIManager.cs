using System.Collections.Generic;
using FruitNinja.Game;
using UnityEngine;
using UnityEngine.UI;

namespace FruitNinja.UI
{
	public class UIManager : MonoBehaviour
	{
	    public static UIManager Instance { get; private set; }

	    [SerializeField] private Text scoreValueText;
	    [SerializeField] private List<Image> healthPool;
		[SerializeField] private Canvas gameOverUI;
		[SerializeField] private Button restartButton;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			GameplayManager.Instance.ScoreChangedEvent += UpdateScoreText;
			GameplayManager.Instance.HealthChangedEvent += UpdateHealth;
			GameplayManager.Instance.GameOverEvent += PrepareGameOverUI;
			
			restartButton.onClick.AddListener(RestartOnClick);
		}

		#region UI operation
		public void UpdateScoreText(int value)
		{
			scoreValueText.text = value.ToString();
		}

		public void UpdateHealth(int value)
		{
			healthPool[value].gameObject.SetActive(false);
		}

		public void PrepareGameOverUI()
		{
			SetActiveGameOverUI(true);
		}

		public void SetActiveGameOverUI(bool value)
		{
			gameOverUI.gameObject.SetActive(value);
		}
		#endregion

		#region button actions
		public void RestartOnClick()
		{
			GameplayManager.Instance.Restart();
		}
		#endregion
	}
}
