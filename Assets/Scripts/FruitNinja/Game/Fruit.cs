using System;
using FruitNinja.Core;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FruitNinja.Game
{
	public class Fruit : MonoBehaviour,IFlyingObject
	{
		public Action<int> AddScore;
		private Type _objType;
		public Type ObjType
		{
			get =>_objType; 
			private set =>_objType=value;
		}
		
		[SerializeField] private int score;
		private void Awake()
		{
			ObjType = Type.FRUIT;
		}
		
		private void Update()
		{
			//Check if the fruit strays too far
			if (Vector3.Distance(transform.position, new Vector3(0, 0, 0)) > 50f)
			{
				gameObject.SetActive(false);
			}
		}

		public void OnClick()
		{
			gameObject.SetActive(false);
			AddScore?.Invoke(score);
		}

		public void Launch(Vector3 initPos,GameObject spawner)
		{
			transform.position = initPos;
			float force = Random.Range(15f, 18f);

			Vector3 launchDirection = Quaternion.AngleAxis(Random.Range(-15f,16f), Vector3.forward) * spawner.transform.up;
			Rigidbody fruitRb = gameObject.GetComponent<Rigidbody>();
			fruitRb.velocity =Vector3.zero;
			gameObject.SetActive(true);
			fruitRb.AddForce(launchDirection * force, ForceMode.Impulse);
			fruitRb.AddTorque(new Vector3(Random.Range(0f, 15f), Random.Range(0f, 15f), Random.Range(0f, 15f)));
		}
	}
}
