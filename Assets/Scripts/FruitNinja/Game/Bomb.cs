using System;
using FruitNinja.Core;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FruitNinja.Game
{
	public class Bomb : MonoBehaviour, IFlyingObject
	{
		public Action<int> AddScore;
		public Action<int> ChangeHealth;
		private Type _objType;

		public Type ObjType
		{
			get => _objType;
			private set => _objType = value;
		}

		[SerializeField] private int score;
		[SerializeField] private int damage;

		private void Awake()
		{
			ObjType = Type.BOMB;
		}

		private void Update()
		{
			//Check if the fruit strays too far
			if (Vector3.Distance(transform.position, new Vector3(0, 0, 0)) > 50f)
			{
				gameObject.SetActive(false);
			}
		}

		public void OnClick()
		{
			gameObject.SetActive(false);
			AddScore?.Invoke(score);
			ChangeHealth?.Invoke(damage);
		}

		public void Launch(Vector3 initPos, GameObject spawner)
		{
			transform.position = initPos;
			float force = Random.Range(15f, 18f);

			Vector3 launchDirection =
				Quaternion.AngleAxis(Random.Range(-15f, 16f), Vector3.forward) * spawner.transform.up;
			Rigidbody bombRb = gameObject.GetComponent<Rigidbody>();
			bombRb.velocity = Vector3.zero;
			gameObject.SetActive(true);
			bombRb.AddForce(launchDirection * force, ForceMode.Impulse);
			bombRb.AddTorque(new Vector3(Random.Range(0f, 15f), Random.Range(0f, 15f), Random.Range(0f, 15f)));
		}
	}
}