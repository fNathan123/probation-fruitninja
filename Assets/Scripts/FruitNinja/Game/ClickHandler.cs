using System;
using FruitNinja.Core;
using UnityEngine;

namespace FruitNinja.Game
{
	public class ClickHandler : MonoBehaviour
	{
		[SerializeField] private Camera mainCamera;

		private void Update()
		{
			if (Input.GetMouseButtonDown(0))
			{
				Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit))
				{
					IFlyingObject obj = hit.transform.GetComponent<IFlyingObject>();
					obj.OnClick();
				}
			}
		}
	}
}