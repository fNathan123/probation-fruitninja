using System;
using System.Collections.Generic;
using FruitNinja.Core;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FruitNinja.Game
{
    public class SpawnerManager : MonoBehaviour
    {
        public static SpawnerManager Instance { get; private set; }
        
        //Events
        public event Action<int> AddScore;
        public event Action<int> ChangeHealth;
        
        [SerializeField] private GameObject fruit1Prefab;
        [SerializeField] private GameObject fruit2Prefab;
        [SerializeField] private GameObject fruit3Prefab;
        [SerializeField] private GameObject bombPrefab;
        [SerializeField] private GameObject pooledObjParent;
        [SerializeField] private List<GameObject> spawners;
        
        private List<GameObject> _fruit1Pool;
        private List<GameObject> _fruit2Pool;
        private List<GameObject> _fruit3Pool;
        private List<GameObject> _bombPool;

        private void Awake()
        {
            Instance = this;
            _fruit1Pool = new List<GameObject>();
            _fruit2Pool = new List<GameObject>();
            _fruit3Pool = new List<GameObject>();
            _bombPool = new List<GameObject>();
        }

        private void Start()
        {
            PoolObject(fruit1Prefab,_fruit1Pool,6);
            PoolObject(fruit2Prefab,_fruit2Pool,6);
            PoolObject(fruit3Prefab,_fruit3Pool,6);
            PoolObject(bombPrefab,_bombPool,10);

            GameplayManager.Instance.GameOverEvent += StopSpawning;
            foreach (var s in spawners)
            {
                InvokeRepeating(nameof(SpawnObject),0.5f,2f);
            }
        }

        private void PoolObject(GameObject prefab, List<GameObject> pool, int count)
        {
            AddScore?.Invoke(100);
            for (var i = 0; i < count; i++)
            {
                var obj = Instantiate(prefab, pooledObjParent.transform);
                obj.SetActive(false);
                var flyingObj = obj.GetComponent<IFlyingObject>();
                
                if (flyingObj.ObjType == Type.FRUIT)
                {
                    var fruit = obj.GetComponent<Fruit>();
                    fruit.AddScore = (x)=>
                    {
                        AddScore?.Invoke(x);
                    };
                }
                else if (flyingObj.ObjType == Type.BOMB)
                {
                    var bomb = obj.GetComponent<Bomb>();
                    bomb.AddScore =(x)=>
                    {
                        AddScore?.Invoke(x);
                    };
                    bomb.ChangeHealth =(x)=>
                    {
                        ChangeHealth?.Invoke(x);
                    };
                }
                
                pool.Add(obj);
            }
        }

        private GameObject GetPooledObject(int obj)
        {
            List<GameObject> pool;
            if (obj == 0)
            {
                pool = _fruit1Pool;
            }
            else if (obj == 1)
            {
                pool = _fruit2Pool;
            }
            else if (obj == 2)
            {
                pool = _fruit3Pool;
            }
            else
            {
                pool = _bombPool;
            }

            for (int i = 0; i < pool.Count; i++)
            {
                if (!pool[i].activeInHierarchy)
                {
                    return pool[i];
                }
            }

            return null;
        }

        public void SpawnObject()
        {
            Debug.Log("SPAWN");
            float rng1 = Random.Range(0f,1f);
            IFlyingObject obj;
            if (rng1 > 0.3f)
            {
                int rng2 = Random.Range(0, 3);
                obj = GetPooledObject(rng2).GetComponent<IFlyingObject>();
            }
            else
            {
                obj = GetPooledObject(4).GetComponent<IFlyingObject>();
            }

            if (obj != null)
            {
                int rng3 = Random.Range(0, spawners.Count);
                obj.Launch(spawners[rng3].transform.position,spawners[rng3]);
            }
        }

        private void StopSpawning()
        {
            CancelInvoke();
        }
    }
    
}
