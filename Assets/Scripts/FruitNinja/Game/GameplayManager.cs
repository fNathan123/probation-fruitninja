using System;
using FruitNinja.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FruitNinja.Game
{
	public class GameplayManager : MonoBehaviour
	{
		public static GameplayManager Instance { get; private set; }

		public GameState State { get; private set; }
		public int Score { get; private set; }
		public int Health { get; private set; }

		public event Action<int> ScoreChangedEvent;
		public event Action<int> HealthChangedEvent;
		public event Action GameOverEvent;

		private void Awake()
		{
			Instance = this;
			State = GameState.GAMEPLAY;
			Health = 3;
		}

		private void Start()
		{
			Debug.Log("gameplay subscibe");
			SpawnerManager.Instance.AddScore += UpdateScore;
			SpawnerManager.Instance.ChangeHealth += UpdateHealth;
		}

		public void GameOver()
		{
			State = GameState.GAMEOVER;
			//UIManager.Instance.SetActiveGameOverUI(true);
			GameOverEvent?.Invoke();
		}

		public void UpdateScore(int value)
		{
			Debug.Log("msk");
			Score += value;
			ScoreChangedEvent?.Invoke(Score);
		}

		public void UpdateHealth(int value)
		{
			Health += value;
			HealthChangedEvent?.Invoke(Health);
			if (Health == 0)
			{
				GameOver();
			}
		}

		public void Restart()
		{
			SceneManager.LoadScene("FruitNinja");
		}
	}
}
