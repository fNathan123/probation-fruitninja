using UnityEngine;

namespace FruitNinja.Core
{
	public interface IFlyingObject
	{
		public Type ObjType { get; }
		public void OnClick();
		public void Launch(Vector3 initPos,GameObject spawner);
	}
}
